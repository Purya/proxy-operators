import psutil
import ranges


def convert_ipv4(ip):
    try:
        return tuple(int(n) for n in ip.split('.'))
    except:
        return (0,0,0,0)
def check_range(addr, start, end):
    
    return convert_ipv4(start) < convert_ipv4(addr) < convert_ipv4(end)

def get_operator(ip):
    for ip_range in ranges.all:
        if check_range(ip,ip_range[0],ip_range[1]) :
            name = ip_range[2].replace("Iran Telecommunication Company PJS","ITC (Mokhaberat)")
            name =        name.replace("Iran Cell Service and Communication Company","IranCell")
            name =        name.replace("Rightel Communication Service Company PJS","Rightel")
            name =        name.replace("Aria Shatel Company Ltd","Shatel")
            name =        name.replace("Asiatech Data Transfer Inc PLC","AsiaTech")
            name =        name.replace("Pars Online PJS","Pars Online")
            name =        name.replace("Mobile Communication Company of Iran PLC","MCI (Hamrah Aval)")
            return name
    return 'Others'
def main():
    datas = {}
    model = "| %-3s | %-50s | %s "
    for c in psutil.net_connections(kind='inet'):
        if c.raddr and c.status == 'ESTABLISHED':
            ip = (c.raddr.ip)
            operator = get_operator(ip)
            datas[operator] = datas[operator] + 1 if operator in datas else 1
    datas = {k: v for k, v in sorted(datas.items(), key=lambda item: item[1],reverse=True)}
    print('\n')
    print('————————————————————————————————————————————————————————————————————————')
    print(model % (
        "ID",
        "Operator",
        "Connections"))
    print('————————————————————————————————————————————————————————————————————————')
    
    alls = 0
    i = 0
    mustbe = ['ITC (Mokhaberat)','IranCell','MCI (Hamrah Aval)','Rightel','AsiaTech','Shatel','Pars Online']
    for x in datas:
        
        alls += datas[x]
        
        if i > 9 : continue
        try: mustbe.remove(x)
        except : pass
        i+=1
        print(model % ( i,  x, datas[x] ))   
    for x in mustbe:
        try:
            i+=1
            print(model % ( i,  x, datas[x] ))   
        except: pass
    print('————————————————————————————————————————————————————————————————————————')
    print(model % ('-','All ',alls ))
    print('\n')
if __name__ == '__main__':
    main()

