# Proxy Operators
A simple script that detects your proxy users operators by ip ranges...

## Usage
Install whit this command :

`cd proxy-operators`
`chmod +x run`
`./run install`

Then launch script with :

`./run`

## TODO

- [x] Remove other operators
- [x] Beutify prints
- [x] Speed up
- [ ] Connecet to telegram bot 
- [x] Make installation file


## Creator 
[@Purya](https://t.me/purya)